# Project Redimor

Project Redimor is an open source project that tries to bring back the way in which Adobe Flash Player was on the web through a new platform, which its code should be written in Python (in addition to that it will be more efficient and easy writing), with this platform it is expected to be able to make applications, games and web animations, exactly as Adobe Flash Player did before.
I hope that this project does not have any copyright issues.

For more information, follow the "Documentacion Redimor" (In Spanish) to see more about the project.
